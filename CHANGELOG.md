# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Generated by [`auto-changelog`](https://github.com/CookPete/auto-changelog).

## [1.0.1](https://gitlab.com/opencity-labs/firenze-smart-log-export/compare/1.0.0...1.0.1)

### Commits

- added ci [`1ffb94d`](https://gitlab.com/opencity-labs/firenze-smart-log-export/commit/1ffb94ddeeb88f4b4dc0a01f5a29cda30f6d4cf5)

## 1.0.0 - 2024-06-18

### Merged

- Log exporter [`#1`](https://gitlab.com/opencity-labs/firenze-smart-log-export/merge_requests/1)

### Fixed

- Merge branch 'log-exporter' into 'main' [`#1`](https://gitlab.com/opencity-labs/firenze-smart-log-export/issues/1)

### Commits

- added log export logic [`dd8703b`](https://gitlab.com/opencity-labs/firenze-smart-log-export/commit/dd8703b948cd14ca3909e9652deceb14640b9d4b)
- fixed kafka event transformation and enriched log with more data [`13a1c4b`](https://gitlab.com/opencity-labs/firenze-smart-log-export/commit/13a1c4b749fa05cc7ba5b813d5370946726a8406)
- added expired case for payment [`f8e7e70`](https://gitlab.com/opencity-labs/firenze-smart-log-export/commit/f8e7e70bc0bee699b15f277e677c0189fc190a9e)
